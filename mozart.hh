#include <cstddef>
#ifdef __GXX_EXPERIMENTAL_CXX0X__
#include <cstdint>
#else
typedef size_t uintptr_t;
#endif
m4_include(«instr.m4»)
m4_include(«instr.cc»)
m4_include(«vtable.m4»)

m4_include(«decl.hh»)
m4_include(«vtable.hh»)
m4_include(«mem.hh»)
m4_include(«gc.hh»)
m4_include(«store.hh»)
m4_include(«threads.hh»)
m4_include(«uniques.hh»)
m4_include(«vm.hh»)
m4_include(«boolean.hh»)
m4_include(«codeBlock.hh»)
m4_include(«abstraction.hh»)
m4_include(«integer.hh»)
m4_include(«builtin.hh»)
m4_include(«simpleVar.hh»)
m4_include(«atom.hh»)
m4_include(«cons.hh»)
