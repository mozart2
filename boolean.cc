void gcIntoBool(Node& it, GC& gc, Node& dest){
  mkBoolean(gc.vm, dest, it.c.ui);
}
const VTable booleanVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«copyable»,1)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoBool»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoBool>»)
  //PP_VTABLE_PRODUCE
};
