class UniqueBlock{
public:
  UniqueBlock* next;
  UniqueBlock* gcTo;
  hash_t hash;
  const VTable* refVT;
  virtual bool is(void* extra)=0;
  virtual void gc(GC& gc)=0;
};
class UniqueController{
  VM& vm;
  size_t capacity;
  size_t occupancy;
  UniqueBlock** tab;
public:
  UniqueController(VM& _vm):vm(_vm),capacity(0),occupancy(0),tab(0){}
  UniqueBlock* get(hash_t hash,const VTable* refVT, void* extra){
    UniqueBlock* c=tab[hash%capacity];
    while(c&&!(c->hash==hash&&c->refVT==refVT&&c->is(extra))){
      c=c->next;
    }
    return c;
  }
  void add(UniqueBlock* n){
    if(capacity>>1<occupancy)grow();
    n->next=tab[n->hash%capacity];
    tab[n->hash%capacity]=n;
  }
  void grow(){
    size_t ocapacity=capacity;
    UniqueBlock** otab=tab;
    capacity=capacity?((capacity<<1)+1):31;
    tab=(UniqueBlock**)operator new (capacity*sizeof(UniqueBlock*),vm);
    for(size_t i=0;i<capacity;++i){
      tab[i]=0;
    }
    for(size_t i=0;i<ocapacity;++i){
      UniqueBlock* c=otab[i];
      while(c){
	add(c);
	c=c->next;
      }
    }
  }
  void gc(){
    capacity=(occupancy<<1)+1;
    tab=(UniqueBlock**)operator new (capacity*sizeof(UniqueBlock*),vm);
    for(size_t i=0;i<capacity;++i){
      tab[i]=0;
    }    
  }
};
void gcIntoUnique(Node& it, GC& gc, Node& dest){
  UniqueBlock* u=(UniqueBlock*)it.c.ptr;
  if(u->gcTo){
    dest.vt=u->refVT;
    dest.c=u->gcTo;
  }else{
    u->gc(gc);
    dest.vt=u->refVT;
    dest.c=u->gcTo;
  }
}

m4_define(«PP_VTABLE_UNIQUE»,«PP_VTABLE_SET(«copyable»,«1») PP_VTABLE_SET(«gcInto»,«&gcIntoUnique») PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoUnique>»)»)
