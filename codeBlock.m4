m4_define(«PPI_Quote»,««««$1»»»»)
m4_define(«PP_CXX_VMA_BEGIN»,«m4_dnl
«»m4_define(«PPI_BC_Instr_Count»,0)m4_dnl
«»PP_FOR_EACH_INSTR(«m4_dnl
«»«»m4_define(m4_defn(«PP_INSTR_NAME»),«m4_dnl
«»«»«»m4_define(«PPI_BC_Instr»PPI_BC_Instr_Count«_Instr»,»PPI_Quote(m4_defn(«PP_INSTR_NAME»))«)m4_dnl
«»«»«»m4_define(«PPI_BC_Instr»PPI_BC_Instr_Count«_Init»,$»«1)m4_dnl
«»«»«»m4_define(«PPI_BC_Instr_Count»,m4_incr(PPI_BC_Instr_Count))m4_dnl
«»«»»)m4_dnl
«»»,«»)m4_dnl
»)m4_dnl
m4_define(«PPI_For_Each_BC_Instr»,«m4_dnl
«»m4_define(«PPI_BC_Instr»,m4_defn(«PPI_BC_Instr$1_Instr»))m4_dnl
«»m4_define(«PPI_BC_Init»,m4_defn(«PPI_BC_Instr$1_Init»))m4_dnl
$3«»m4_dnl
m4_ifelse(«$1»,«$2»,«»,«PPI_For_Each_BC_Instr(m4_incr($1),«$2»,«$3»)»)m4_dnl
»)m4_dnl
m4_define(«PPI_FOR_EACH_BC_INSTR»,«m4_dnl
PPI_For_Each_BC_Instr(0,m4_decr(PPI_BC_Instr_Count),«$1»)
»)m4_dnl
m4_define(«PP_CXX_VMA_END»,«m4_dnl
«void »$1«(VM &vm, Node& target, size_t arity, size_t numG){
  target.vt=&codeBlockVT;
  size_t size=0»m4_dnl
PPI_FOR_EACH_BC_INSTR(«m4_dnl
«+sizeof(s_»PPI_BC_Instr«)»m4_dnl
»)m4_dnl
«;
  target.c.ptr=operator new(sizeof(CodeBlockHeader)+size, vm);
  CodeBlockHeader* h=(CodeBlockHeader*)target.c.ptr;
  h->length=size;
  h->arity=arity;
  h->numG=numG;
  Instr* w=(Instr*)(h+1);
»m4_dnl
PPI_FOR_EACH_BC_INSTR(«m4_dnl
  {
    s_«»PPI_BC_Instr &i=*(s_«»PPI_BC_Instr*)w;
    i.instr=vm.instrTable[I_«»PPI_BC_Instr];
    PPI_BC_Init
  }
  w+=sizeof(s_«»PPI_BC_Instr)/sizeof(Instr);
»)m4_dnl
«
}
»m4_dnl
»)m4_dnl