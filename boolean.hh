extern const VTable booleanVT;
template<class T>
T bTest(Node &c,T t, T f, T nb){
  Node &cd=deref(c);
  if(cd.vt!=&booleanVT)return nb;
  if(cd.c.ui)return t;
  return f;
}
void mkBoolean(VM&, Node &target, bool value){
  target.vt=&booleanVT;
  target.c.ui=value?1:0;
}
