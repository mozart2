void gcIntoSmallInt(Node& it, GC& gc, Node& dest){
  mkSmallint(gc.vm, dest, it.c.si);
}
const VTable smallintVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«copyable»,«1»)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoSmallInt»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoSmallInt>»)
  //PP_VTABLE_PRODUCE
};
