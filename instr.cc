//Types of parameters: WX RX WY RY RG V X[N] S I PA LX
//PP_DEF_INSTR(«copyXX»,«
//PP_INSTR_PARAM(«dest»,«WX»)
//PP_INSTR_PARAM(«src»,«RX»)
//»,«
{set(*i.dest, *i.src, vm);}
//»)
//PP_DEF_INSTR(«copyXY»,«
//PP_INSTR_PARAM(«dest»,«WX»)
//PP_INSTR_PARAM(«src»,«RY»)
//»,«
{set(*i.dest, Y[i.src], vm);}
//»)
//PP_DEF_INSTR(«copyXG»,«
//PP_INSTR_PARAM(«dest»,«WX»)
//PP_INSTR_PARAM(«src»,«RG»)
//»,«
{set(*i.dest, G[i.src]);}
//»)
//PP_DEF_INSTR(«copyXC»,«
//PP_INSTR_PARAM(«dest»,«WX»)
//PP_INSTR_PARAM(«src»,«V»)
//»,«
{set(*i.dest, i.src);}
//»)
//PP_DEF_INSTR(«copyYX»,«
//PP_INSTR_PARAM(«dest»,«WY»)
//PP_INSTR_PARAM(«src»,«RX»)
//»,«
{set(Y[i.dest], *i.src, vm);}
//»)
//PP_DEF_INSTR(«movYX»,«
//PP_INSTR_PARAM(«dest»,«WY»)
//PP_INSTR_PARAM(«src»,«RX»)
//»,«
{move(Y[i.dest], *i.src);}
//»)
//PP_DEF_INSTR(«movXY»,«
//PP_INSTR_PARAM(«dest»,«WX»)
//PP_INSTR_PARAM(«src»,«RY»)
//»,«
{move(*i.dest, Y[i.src]);}
//»)
//PP_DEF_INSTR(«callBI0»,«
//PP_INSTR_PARAM(«bi»,«V»)
//»,«
{
  StableNode& _bi=deref(i.bi);
  if((st=_bi.vt->biProc(_bi, vm, 0)))goto specialStatus;
}
//»)
//m4_define(«CALL_BI»,«
//PP_DEF_INSTR(«callBI$1»,«
//PP_INSTR_PARAM(«liveXs»,«LX»)
//PP_INSTR_PARAM(«bi»,«V»)
//PP_INSTR_PARAM(«params»,«X[$1]»)
//»,«
{
  StableNode& _bi=deref(i.bi);
  if((st=_bi.vt->biProc(_bi, vm, i.params)))goto specialStatus;
}
//»)
//»)
CALL_BI(1)
CALL_BI(2)
CALL_BI(3)
CALL_BI(4)
CALL_BI(5)
CALL_BI(6)
//PP_DEF_INSTR(«putX»,«
//PP_INSTR_PARAM(«src»,«RX»)
//»,«
{set(*(wPtr++),*i.src);}
//»)
//PP_DEF_INSTR(«putY»,«
//PP_INSTR_PARAM(«src»,«RY»)
//»,«
{set(*(wPtr++),Y[i.src]);}
//»)
//PP_DEF_INSTR(«putG»,«
//PP_INSTR_PARAM(«src»,«RG»)
//»,«
{set(*(wPtr++),G[i.src]);}
//»)
//PP_DEF_INSTR(«putC»,«
//PP_INSTR_PARAM(«src»,«V»)
//»,«
{set(*(wPtr++),i.src);}
//»)
//PP_DEF_INSTR(«allocateY»,«
//PP_INSTR_PARAM(«count»,«S»)
//»,«
{
  Y=alloc<UnstableNode>(i.count);
  numY=i.count;
  for(size_t i=0;i<numY;++i){
    new((void*)&Y[i]) UnstableNode();
  }
}
//»)
//PP_DEF_INSTR(«deallocateY»,«
//PP_INSTR_PARAM(«liveXs»,«LX»)
//»,«
{
  free(Y,numY);
  Y=0;
  numY=0;
}
//»)
//PP_DEF_INSTR(«doPop»,«
//PP_INSTR_PARAM(«liveXs»,«LX»)
//»,«
{if(!stackTop)goto stackEmpty; goto *(stackTop->kind);}
//»)
//PP_DEF_INSTR(«jump»,«
//PP_INSTR_PARAM(«liveXs»,«LX»)
//PP_INSTR_PARAM(«to»,«I»)
//»,«
{
  IP=i.to;
}
//»)
//PP_DEF_INSTR(«ifX»,«
//PP_INSTR_PARAM(«liveXs»,«LX»)
//PP_INSTR_PARAM(«cond»,«RX»)
//PP_INSTR_PARAM(«ifFalse»,«I»)
//PP_INSTR_PARAM(«ifNoBool»,«I»)
//»,«
{
  IP=bTest(*(i.cond), IP, i.ifFalse, i.ifNoBool);
}
//»)
//PP_DEF_INSTR(«callX»,«
//PP_INSTR_PARAM(«callee»,«RX»)
//PP_INSTR_PARAM(«arity»,«PA»)
//»,«
{
  ContStackFrame *f=alloc<ContStackFrame>(1);
  f->kind=&&stackCont;
  f->next=stackTop;
  f->startIP=startIP;
  f->IP=IP;
  f->numY=numY;
  f->Y=Y;
  f->G=G;
  stackTop=f;
  Y=0;
  numY=0;
  Node &n=deref(*(i.callee));
  Instr* nIP;
  StableNode* nG;
  n.vt->call(n, nIP, nG, i.arity);
  startIP=nIP;
  IP=nIP;
  G=nG;
}
//»)
//PP_DEF_INSTR(«tailCallX»,«
//PP_INSTR_PARAM(«callee»,«RX»)
//PP_INSTR_PARAM(«arity»,«PA»)
//»,«
{
  Node &n=deref(*(i.callee));
  Instr* nIP;
  StableNode* nG;
  n.vt->call(n, nIP, nG, i.arity);
  startIP=nIP;
  IP=nIP;
  G=nG;
}
//»)
//PP_DEF_INSTR(«createVarXY»,«
//PP_INSTR_PARAM(«dest1»,«WX»)
//PP_INSTR_PARAM(«dest2»,«WY»)
//»,«
{
  StableNode* n=new(vm) StableNode();
  mkSimpleVar(vm,*n);
  set(*i.dest1,*n);
  set(Y[i.dest2],*n);
}
//»)
