const size_t X_COUNT=1<<8;
const size_t INSTR_COUNT=PP_INSTR_COUNT;
enum StackFrames{
  S_CONT,
  S_GC_CONT,
  S_X,
  STACK_COUNT
};
struct StackFrameHeader{
  StackFrameHeader* next;
  Instr kind;
};
struct ContStackFrame:public StackFrameHeader{
  Instr* startIP;
  Instr* IP;
  size_t numY;
  UnstableNode* Y;
  StableNode* G;
};
struct GCContStackFrame:public StackFrameHeader{
  size_t offset;
  size_t numY;
  UnstableNode* Y;
  UnstableNode abstr;
};
//struct LocalStackFrame:public StackFrameHeader{};
struct SavedX{
  size_t x;
  UnstableNode r;
};
struct XStackFrame:public StackFrameHeader{
  size_t count;
  SavedX* ptr;
};
class VM{
public:
  VM(BlockAllocator &ba,
     bool (*_scheduleNext)(),
     bool (*_interrupted)() ):
    instrTable(),
    stackTable(),
    X(),
    uniques(*this),
    active(ba),
    shadow(ba),
    bigEmulAlloc(),
    smallEmulAlloc(),
    bigEmulAllocShadow(),
    smallEmulAllocShadow(),
    threads(),
    initialized(0),
    scheduleNext(_scheduleNext),
    interrupted(_interrupted)
  {run();}
  bool run();
  void pushCall(Node &thread, Node& proc);
  void schedule(UnstableNode &thread){
    threads.schedule(*this, thread);
  }
  void gc();
  void gcStack(GC &gc, StackFrameHeader* &from, StackFrameHeader* &to);
  Instr instrTable[INSTR_COUNT];
  Instr stackTable[STACK_COUNT];
  UnstableNode X[X_COUNT];
  UniqueController uniques;
private:
  friend void* operator new(size_t s, VM& vm);
  MemPool active;
  MemPool shadow;
  FastAllocator<4,6> bigEmulAlloc;
  FastAllocator<0,6> smallEmulAlloc;
  FastAllocator<4,6> bigEmulAllocShadow;
  FastAllocator<0,6> smallEmulAllocShadow;
  template<class T> T* alloc(size_t count){
    size_t s=((count*sizeof(T))+(sizeof(void*)-1))/sizeof(void*);
    if(s<(1<<6)){
      return (T*)smallEmulAlloc.alloc(active,s);
    }else{
      return (T*)bigEmulAlloc.alloc(active,s);
    }
  }
  template<class T> void free(T* ptr, size_t count){
    size_t s=((count*sizeof(T))+(sizeof(void*)-1))/sizeof(void*);
    if(s<(1<<6)){
      smallEmulAlloc.free((char*)ptr,s);
    }else{
      bigEmulAlloc.free((char*)ptr,s);
    }
  }
  template<class T> T* allocShadow(size_t count){
    size_t s=((count*sizeof(T))+(sizeof(void*)-1))/sizeof(void*);
    if(s<(1<<6)){
      return (T*)smallEmulAllocShadow.alloc(shadow,s);
    }else{
      return (T*)bigEmulAllocShadow.alloc(shadow,s);
    }
  }
  template<class T> void freeShadow(T* ptr, size_t count){
    size_t s=((count*sizeof(T))+(sizeof(void*)-1))/sizeof(void*);
    if(s<(1<<6)){
      smallEmulAllocShadow.free((char*)ptr,s);
    }else{
      bigEmulAllocShadow.free((char*)ptr,s);
    }
  }
  friend class ThreadPool;
  ThreadPool threads;
  bool initialized;
  bool (*scheduleNext)();
  bool (*interrupted)();
};
void* operator new(size_t s, VM& vm){
  return vm.active.alloc(s,DEFAULT_ALIGNMENT);
}
//PP_FOR_EACH_INSTR(«
struct s_«»PP_INSTR_NAME {
  Instr instr;
  PP_FOR_EACH_INSTR_PARAM(«
  m4_ifelse(m4_defn(«PP_INSTR_PARAM_TYPE»),«WX»,«UnstableNode* »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«RX»,«UnstableNode* »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«WY»,«size_t »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«RY»,«size_t »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«RG»,«size_t »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«V»,«StableNode »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«S»,«size_t »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«I»,«Instr* »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«PA»,«size_t »PP_INSTR_PARAM_NAME«;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«LX»,«long »PP_INSTR_PARAM_NAME«;»,
	    m4_regexp(m4_defn(«PP_INSTR_PARAM_TYPE»),«X\[\([0-9]*\)\]»,«UnstableNode* »PP_INSTR_PARAM_NAME«[\1];»))»,«»)
};
//»,«»)
enum Instructions{
PP_FOR_EACH_INSTR(«I_«»PP_INSTR_NAME»,«,»)
};
