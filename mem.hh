typedef char byte;
const size_t DEFAULT_BLOCK_SIZE=1<<24;
const size_t DEFAULT_ALIGNMENT=16;
const size_t MIN_SMALL_BLOCK=1<<12;
const size_t SMALL_MEM=1<<10;
struct MemoryBlock{
  MemoryBlock* next;
  byte* pos;
  byte* pastEnd;
};
class BlockAllocator{
public:
  virtual MemoryBlock* request(size_t size, size_t alignment)=0;
  virtual void release(MemoryBlock* block)=0;
};
class MemPool{
  friend void exchange(MemPool &a, MemPool &b);
  BlockAllocator &ba;
  MemoryBlock* spentBlocks;
  MemoryBlock* smallBlocks;
  byte* curSmallPos;
  byte* curSmallPastEnd;
  MemoryBlock* curSmall;
  MemoryBlock* curBig;
public:
  byte* smallalloc(size_t size, size_t alignment){
    uintptr_t cur=(uintptr_t)curSmallPos;
    if(cur&(alignment-1)){
      cur&=~(alignment-1);
      cur+=alignment;
      if(cur>(uintptr_t)curSmallPastEnd)cur=(uintptr_t)curSmallPastEnd;
    }
    curSmallPos=(byte*)cur;
    if(((size_t)(curSmallPastEnd-curSmallPos))>=size){
      byte* ret=curSmallPos;
      curSmallPos+=size;
      return ret;
    }else{
      if(curSmall){
	curSmall->next=spentBlocks;
	spentBlocks=curSmall;
      }
      if(smallBlocks){
	curSmall=smallBlocks;
	smallBlocks=smallBlocks->next;
      }else{
	curSmall=ba.request(DEFAULT_BLOCK_SIZE, DEFAULT_ALIGNMENT);
      }
      curSmallPos=curSmall->pos;
      curSmallPastEnd=curSmall->pastEnd;
      return smallalloc(size, alignment);
    }
  }
  byte* bigalloc(size_t size, size_t alignment){
    uintptr_t cur=(uintptr_t)curBig->pos;
    if(cur&(alignment-1)){
      cur&=~(alignment-1);
      cur+=alignment;
    }    
    curBig->pos=(byte*)cur;
    if(curBig->pos<=curBig->pastEnd-size){
      byte* ret=curBig->pos;
      curBig->pos+=size;
      return ret;
    }else{
      if(curBig->pos<=curBig->pastEnd-MIN_SMALL_BLOCK){
	curBig->next=smallBlocks;
	smallBlocks=curBig;
      }else{
	curBig->next=spentBlocks;
	spentBlocks=curBig;
      }
      if((size+alignment<=DEFAULT_BLOCK_SIZE)){
	curBig=ba.request(DEFAULT_BLOCK_SIZE, DEFAULT_ALIGNMENT);
      }else{
	curBig=ba.request(size, alignment);
      }
      return bigalloc(size, alignment);
    }
  }
public:
  MemPool(BlockAllocator &_ba):ba(_ba),spentBlocks(0),smallBlocks(0),
			       curSmallPos(0),curSmallPastEnd(0),curSmall(0),curBig(0){};
  byte* alloc(size_t size, size_t alignment){
    if(size<=SMALL_MEM)return smallalloc(size?size:1, alignment);
    return bigalloc(size, alignment);
  }
  void release(){
    while(spentBlocks){MemoryBlock* todo=spentBlocks; spentBlocks=spentBlocks->next; ba.release(todo);}
    while(smallBlocks){MemoryBlock* todo=smallBlocks; smallBlocks=smallBlocks->next; ba.release(todo);}
    if(curSmall){ba.release(curSmall);curSmall=0;curSmallPos=0;curSmallPastEnd=0;}
    if(curBig){ba.release(curBig);curBig=0;}
  }
};
void exchange(MemPool &a, MemPool &b){
  MemoryBlock* tmp;
  tmp=a.spentBlocks;
  a.spentBlocks=b.spentBlocks;
  b.spentBlocks=tmp;
  tmp=a.smallBlocks;
  a.smallBlocks=b.smallBlocks;
  b.smallBlocks=tmp;
  tmp=a.curSmall;
  a.curSmall=b.curSmall;
  b.curSmall=tmp;
  tmp=a.curBig;
  a.curBig=b.curBig;
  b.curBig=tmp;
  byte* tmp2;
  tmp2=a.curSmallPos;
  a.curSmallPos=b.curSmallPos;
  b.curSmallPos=tmp2;
  tmp2=a.curSmallPastEnd;
  a.curSmallPastEnd=b.curSmallPastEnd;
  b.curSmallPastEnd=tmp2;
}
void* operator new(size_t s, MemPool &m){
  return m.alloc(s,DEFAULT_ALIGNMENT);
}
template<size_t Levels, size_t BitWidth>
class FastAllocator;
template<size_t Levels, size_t BitWidth>
void exchange(FastAllocator<Levels,BitWidth> &a, FastAllocator<Levels,BitWidth> &b);
template<size_t Levels, size_t BitWidth>
class FastAllocator{
  FastAllocator<Levels-1,BitWidth>* tab[1<<BitWidth];
public:
  friend void exchange<Levels,BitWidth>(FastAllocator&,FastAllocator&);
  typedef FastAllocator<Levels-1,BitWidth>* Element;
  FastAllocator(){
    clear();
  }
  char* alloc(MemPool &m, size_t s){
    size_t i=(s&(((1<<BitWidth)-1)<<(Levels*BitWidth)))>>(Levels*BitWidth);
    if(!tab[i]) tab[i]=new(m) FastAllocator<Levels-1,BitWidth>();
    return tab[i]->alloc(m, s);
  }
  void free(char* p, size_t s){
    size_t i=(s&(((1<<BitWidth)-1)<<(Levels*BitWidth)))>>(Levels*BitWidth);
    tab[i]->free(p,s);
  }
  void clear(){
    for(int i=0;i<1<<BitWidth;++i){
      tab[i]=0;
    }
  }
};
template<size_t BitWidth>
class FastAllocator<0,BitWidth>{
  char* tab[1<<BitWidth];
public:
  friend void exchange<0,BitWidth>(FastAllocator&,FastAllocator&);
  typedef char* Element;
  FastAllocator(){
    clear();
  }
  char* alloc(MemPool &m, size_t s){
    size_t i=s&((1<<BitWidth)-1);
    if(tab[i]){
      char* r=tab[i];
      tab[i]=*((char**)tab[i]);
      return r;
    }else{
      return m.alloc((s?s:1)*sizeof(void*),DEFAULT_ALIGNMENT);
    }
  }
  void free(char* p, size_t s){
    size_t i=s&((1<<BitWidth)-1);
    *((char**)p)=tab[i];
    tab[i]=p;
  }
  void clear(){
    for(int i=0;i<1<<BitWidth;++i){
      tab[i]=0;
    }
  }
};
template<size_t Levels, size_t BitWidth>
void exchange(FastAllocator<Levels,BitWidth> &a, FastAllocator<Levels,BitWidth> &b){
  for(int i=0;i<1<<BitWidth;++i){
    typename FastAllocator<Levels,BitWidth>::Element tmp=a.tab[i];
    a.tab[i]=b.tab[i];
    b.tab[i]=tmp;
  }
}
