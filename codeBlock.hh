struct CodeBlockHeader{
  size_t length;
  size_t arity;
  size_t numG;
};
extern const VTable codeBlockVT;
