extern const VTable consVT;
struct Cons{
  StableNode v[2];
};
StableNode* mkCons(VM& vm, Node& dest){
  dest.vt=&consVT;
  dest.c.ptr=new(vm) Cons();
  return ((Cons*)dest.c.ptr)->v;
}
