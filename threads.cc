void ThreadPool::get(VM &vm, UnstableNode &dest){
  if(front){
    move(dest,front->thread);
    ThreadList* nF=front->next;
    vm.free(front,1);
    front=nF;
    if(!front)back=&front;
  }else{
    clobber(dest);
  }
}
void ThreadPool::schedule(VM &vm, UnstableNode &t){
  ThreadList* l=vm.alloc<ThreadList>(1);
  l->next=0;
  move(l->thread,t);
  *back=l;
  back=&(l->next);
}
void ThreadPool::gc(GC &gc){
  ThreadList* o=front;
  ThreadList** n=&front;
  while(o){
    *n=vm.alloc<ThreadList>(1);
    (*n)->next=0;
    new((void*)&((*n)->thread)) UnstableNode(gc,o->thread);
    n=&((*n)->next);
    o=o->next;
  }
  back=n;
}
StackFrameHeader*& getStackTop(Node& it){
  return ((Thread*)it.c.ptr)->topOfStack;
}
void gcIntoThread(Node& it, GC& gc, Node& dest){
  mkThread(gc.vm, dest);
  gc.vm.gcStack(gc, getStackTop(it), getStackTop(dest));
}
const VTable threadVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«stackTop»,«&getStackTop»)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoThread»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoThread>»)
  //PP_VTABLE_PRODUCE
};
