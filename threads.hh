struct ThreadList{
  UnstableNode thread;
  ThreadList* next;
};
class ThreadPool{
private:
  ThreadList* front;
  ThreadList** back;
public:
  ThreadPool():front(0),back(&front){}
  void get(VM &vm, UnstableNode& dest);
  void schedule(VM &vm, UnstableNode& t);
  void gc(GC& gc);
};
class Thread{
  StackFrameHeader* topOfStack;
  friend StackFrameHeader*& getStackTop(Node& it);
public:
  Thread():topOfStack(0){}
};
extern const VTable threadVT;
void mkThread(VM &vm,Node &target){
  target.vt=&threadVT;
  target.c.ptr=new(vm)Thread();
}
