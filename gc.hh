struct GCLink{
  GCLink* next;
  StableNode** nptr;
  GCLink(GCLink* _next, StableNode** _nptr):next(_next),nptr(_nptr){}
};
struct GC{
  GC(VM& _vm):stableTodo(0),unstableTodo(0),ptrStableTodo(0),vm(_vm){}
  StableNode* stableTodo;
  UnstableNode* unstableTodo;
  GCLink* ptrStableTodo;
  VM& vm;
  void registerPointer(StableNode*& nptr){
    ptrStableTodo=new/*(gcTempMem)*/GCLink(ptrStableTodo,&nptr);
  }
  void process();
};
extern const VTable gcStableRefVT, gcUnstableRefVT;
template<StableNode& (*GC_FUNC)(Node&,GC&)>
void gcIntoDef(Node& it,GC& gc, Node& dest);
template<void (*GC_INTO_FUNC)(Node&,GC&,Node&)>
StableNode& gcDef(Node& it,GC& gc);
