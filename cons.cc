void gcIntoCons(Node& it, GC& gc, Node& dest){
  dest.vt=&consVT;
  Cons& c=*(Cons*)new(vm) Cons();
  Cons& oc=*(Cons*)it.c.ptr;
  dest.c.ptr=&c;
  new(&c.v[0]) StableNode(gc,oc.v[0]);
  new(&c.v[1]) StableNode(gc,oc.v[1]);
}
const VTable consVT={
  //PP_VTABLE_DEF(vtUnimplemented)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoCons»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoCons>»)
  //PP_VTABLE_PRODUCE
};
