typedef unsigned int umw;
typedef signed int smw;
typedef float fmw;

union MemWord{
  void* ptr;
  void(*fptr)(void);
  umw ui;
  smw si;
  fmw f;
  MemWord(void* _ptr):ptr(_ptr){}
};
struct Node{
  union{
    const VTable* vt;
    Node* gcNext;
  };
  MemWord c;
  Node(VTable* _vt, MemWord _c):vt(_vt),c(_c){}
  Node(Node* gcList, Node& orig):gcNext(gcList),c((void*)&orig){}
  Node():vt(&vtNoValue), c(0){}
};
struct StableNode: public Node{
  StableNode(GC& gc,Node& orig):Node(gc.stableTodo,orig){
    gc.stableTodo=this;
  }
  StableNode(VTable* _vt, MemWord _c):Node(_vt,_c){}
  StableNode(){}
};
/*An unstable node contains a conceptual node value but a pointer to it doesn't correspond to any specific conceptual node. They can't be the target of Ref nodes. Typical examples are cells or VM registers.*/
struct UnstableNode: public Node{
  UnstableNode(GC& gc,Node& orig):Node(gc.unstableTodo,orig){
    gc.unstableTodo=this;
  }
  UnstableNode(){}
};
struct VTable{
  //PP_VTABLE_DECL(«copyable»)
  bool copyable;
  //PP_VTABLE_DECL(«gc»)
  StableNode& (*gc)(Node& it, GC& gc);
  //PP_VTABLE_DECL(«gcInto»)
  void (*gcInto)(Node& it, GC& gc, Node& dest);
  //PP_VTABLE_DECL(«stackTop»)
  StackFrameHeader*& (*stackTop)(Node& it);
  //PP_VTABLE_DECL(«biProc»)
  BIStatus* (*biProc)(Node& it, VM &vm, UnstableNode* args[]);
  //PP_VTABLE_DECL(«call»)
  void (*call)(Node& it, Instr* &IP, StableNode* &G, size_t arity);
};
Node& deref(Node& n){
  Node* np=&n;
  while(!np->vt)np=(Node*)np->c.ptr;
  return *np;
}
StableNode& deref(StableNode& n){
  StableNode* np=&n;
  while(!np->vt)np=(StableNode*)np->c.ptr;
  return *np;
}
StableNode& stable(UnstableNode& n, VM& vm){
  if(n.vt){
    StableNode* s=new(vm) StableNode();
    set(*s,n);
    return *s;
  }else{
    return (StableNode&)deref(n);
  }
}
void set(Node& dest, StableNode& src){
  if(src.vt&&!src.vt->copyable){
    dest.vt=0;
    dest.c.ptr=&src;
  }else{
    dest.vt=src.vt;
    dest.c=src.c;
  }
}
void set(StableNode& dest, UnstableNode& src){
  dest.vt=src.vt;
  dest.c=src.c;
  if(src.vt&&!src.vt->copyable){
    src.vt=0;
    src.c.ptr=&dest;
  }
}
void set(UnstableNode& dest, UnstableNode& src, VM& vm){
  if(src.vt&&src.vt->copyable){
    dest.vt=src.vt;
    dest.c=src.c;
  }else{
    set(dest,stable(src,vm));
  }
}
void move(UnstableNode& dest, UnstableNode& src){
  dest.vt=src.vt;
  dest.c=src.c;
  src.vt=&vtNoValue;
}
void clobber(Node& dest){
  dest.vt=&vtNoValue;
}
