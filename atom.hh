#include <cstring>
hash_t hash(char* data){
  hash_t h=0;
  while(*data){h=(h<<1)+h; h^=*data;}
  return h;
}
extern const VTable atomVT;
struct AtomData{
  size_t length;
  char* data;
};
class Atom:public UniqueBlock{
public:
  bool is(void* extra){
    AtomData &d=*(AtomData*)extra;
    return d.length==length && !memcmp(data,d.data,length);
  }
  void gc(GC& gc){
    Atom* a=new(gc.vm) Atom();
    a->next=0;
    a->gcTo=0;
    a->hash=hash;
    a->refVT=&atomVT;
    a->length=length;
    a->data=(char*)operator new(length+1,gc.vm);
    memcpy(a->data,data,length);
    a->data[length+1]='\0';
    gc.vm.uniques.add(a);
    gcTo=a;
  }
  size_t length;
  char* data;
};
void mkAtom(VM &vm,Node &dest,char* data){
  hash_t h=hash(data);
  AtomData d;
  d.length=strlen(data);
  d.data=data;
  Atom* a=(Atom*)vm.uniques.get(h,&atomVT,&d);
  if(!a){
    a=new(vm)Atom();
    a->next=0;
    a->gcTo=0;
    a->hash=h;
    a->refVT=&atomVT;
    a->length=d.length;
    a->data=(char*)operator new(d.length+1,vm);
    memcpy(a->data,data,d.length);
    a->data[d.length+1]='\0';
    vm.uniques.add(a);
  }
  dest.vt=&atomVT;
  dest.c.ptr=a;
}
