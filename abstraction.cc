void callAbstr(Node& it, Instr* &IP, StableNode* &G, size_t arity){
  Abstraction& a=*(Abstraction*)it.c.ptr;
  G=a.G;
  StableNode& cb=deref(a.code);
  IP=(arity==(size_t)-1||arity==a.arity)?(Instr*)(((CodeBlockHeader*)cb.c.ptr)+1):0;
}
StableNode& gcAbstr(Node& it, GC& gc){
  Abstraction* a=(Abstraction*)it.c.ptr;
  Abstraction* abstr=new(gc.vm) Abstraction(gc,a);
  return *((abstr->G)-1);
}
const VTable abstractionVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«gc»,«&gcAbstr»)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoDef<&gcAbstr>»)
  //PP_VTABLE_SET(«call»,«&callAbstr»)
  //PP_VTABLE_PRODUCE
};
Abstraction::Abstraction(GC& gc, Abstraction* a):arity(a->arity), numG(a->numG), code(gc,a->code){
  StableNode* Gminus1=(StableNode*)operator new(sizeof(StableNode)*(1+numG),vm);
  G=Gminus1+1;
  Gminus1->vt=&abstractionVT;
  Gminus1->c.ptr=this;
  for(unsigned int i=0;i<numG;++i){
    new((void*)&G[i]) StableNode(gc, a->G[i]);
  }
}
StableNode* mkAbstraction(VM &vm, Node &target, StableNode &cb, size_t arity, size_t numG){
  Abstraction* abstr=new(vm) Abstraction(arity,numG);
  StableNode* Gminus1=(StableNode*)operator new(sizeof(StableNode)*(1+numG),vm);
  set(abstr->code, cb);
  abstr->G=Gminus1+1;
  abstr->numG=numG;
  abstr->arity=arity;
  Gminus1->vt=&abstractionVT;
  Gminus1->c.ptr=abstr;
  set(target,*Gminus1);
  return abstr->G;
}
