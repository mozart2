BIStatus* simpleBuiltinExec(Node &it, VM &vm, UnstableNode* args[]){
  return ((BIStatus*(*)(VM &vm, UnstableNode*  args[]))(it.c.fptr))(vm, args);
}
void gcIntoSimpleBuiltin(Node& it, GC& gc, Node& dest){
  mkBuiltin(gc.vm, dest, (BIStatus*(*)(VM&,UnstableNode*[]))it.c.fptr);
}
const VTable simpleBuiltinVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoSimpleBuiltin»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoSimpleBuiltin>»)
  //PP_VTABLE_SET(«biProc»,&simpleBuiltinExec)
  //PP_VTABLE_PRODUCE
};
