#include <iostream>
#include <cstdlib>
m4_include(«mozart.hh»)
int memAllocs=0;
class NewBlockAllocator: public BlockAllocator {
  MemoryBlock* request(size_t size, size_t alignment){
    if(!--memAllocs){std::cerr<<"out of memory"<<std::endl; exit(1);}
    MemoryBlock* m=(MemoryBlock*) operator new(size+alignment+sizeof(MemoryBlock));
    m->pos=((byte*)m)+sizeof(MemoryBlock);
    m->pastEnd=((byte*)m)+size+alignment+sizeof(MemoryBlock);
    return m;
  }
  void release(MemoryBlock* block){
    operator delete(block);
  }
} blocks;
bool sched(){static size_t f=0; return ((++f)%193)==0;}
bool inter(){static size_t f=0; return ((++f)%193)==0;}
void mkMainCode(VM&,Node&,size_t,size_t);
void mkFibonacci(VM&,Node&,size_t,size_t);
void mkNaiveFibonacci(VM&,Node&,size_t,size_t);
VM vm=VM(blocks, sched, inter);
int main(int argc, char* argv[]){
  UnstableNode& mainThread=*new(vm)UnstableNode();
  mkThread(vm, mainThread);
  StableNode& mainCode=*new(vm)StableNode();
  mkMainCode(vm,mainCode,0,3);
  StableNode& mainProc=*new(vm)StableNode();
  StableNode* gMainProc=mkAbstraction(vm,mainProc,mainCode,0,3);
  mkSmallint(vm,*gMainProc++,37);

  StableNode& fibCode=*new(vm)StableNode();
  mkFibonacci(vm,fibCode,5,0);
  mkAbstraction(vm,*gMainProc++,fibCode,5,0);
  
  StableNode& naiveFibCode=*new(vm)StableNode();
  mkNaiveFibonacci(vm,naiveFibCode,2,1);
  StableNode& naiveFibProc=*new(vm)StableNode();
  StableNode* gNaiveFibProc=mkAbstraction(vm,naiveFibProc,naiveFibCode,2,1);
  set(*gMainProc++,naiveFibProc);
  set(*gNaiveFibProc++,naiveFibProc);

  vm.pushCall(mainThread,mainProc);
  vm.schedule(mainThread);
  while(vm.run()){vm.gc();}
  return 0;
}

BIStatus* intLess(VM&,UnstableNode* args[]){
  Node &n0=deref(*args[0]);
  Node &n1=deref(*args[1]);
  if(n0.vt!=&smallintVT)std::cout<<"n0 is not an integer"<<std::endl;
  if(n1.vt!=&smallintVT)std::cout<<"n1 is not an integer"<<std::endl;
  mkBoolean(vm,*args[2],n0.c.si<n1.c.si);
  return 0;
}
BIStatus* intSum(VM&,UnstableNode* args[]){
  Node &n0=deref(*args[0]);
  Node &n1=deref(*args[1]);
  if(n0.vt!=&smallintVT)std::cout<<"n0 is not an integer"<<std::endl;
  if(n1.vt!=&smallintVT)std::cout<<"n1 is not an integer"<<std::endl;
  mkSmallint(vm,*args[2],n0.c.si+n1.c.si);
  return 0;
}
BIStatus* printInt(VM&,UnstableNode* args[]){
  Node &n0=deref(*args[0]);
  if(n0.vt!=&smallintVT)std::cout<<"n0 is not an integer"<<std::endl;
  std::cout<<n0.c.si<<std::endl;
  return 0;
}
BIStatus* unify(VM&,UnstableNode* args[]){
  Node &n0=deref(*args[0]);
  Node &n1=deref(*args[1]);
  if(n0.vt!=&simpleVarVT)std::cout<<"n0 is not an unbound variable"<<std::endl;  
  if(&n1!=args[1])
    set(n0,(StableNode&)n1);
  else
    if(&n0==args[0])
      set((UnstableNode&)n0,(UnstableNode&)n1,vm);
    else
      set((StableNode&)n0,(UnstableNode&)n1);
  return 0;
}
BIStatus* makeVar(VM&vm,UnstableNode* args[]){
  mkSimpleVar(vm,*args[0]);
  return 0;
}
m4_include(«gc.cc»)
m4_include(«vm.cc»)
m4_include(«threads.cc»)
m4_include(«vtable.cc»)
m4_include(«boolean.cc»)
m4_include(«codeBlock.cc»)
m4_include(«abstraction.cc»)
m4_include(«integer.cc»)
m4_include(«builtin.cc»)
m4_include(«simpleVar.cc»)
m4_include(«atom.cc»)
m4_include(«cons.cc»)

m4_include(«codeBlock.m4»)

Instr** else1;
PP_CXX_VMA_BEGIN()
allocateY(«i.count=1;»)
callBI1(«mkBuiltin(vm,i.bi,&printInt); i.params[0]=&vm.X[1]; i.liveXs=31;»)
copyYX(«i.dest=0; i.src=&vm.X[1];»)
callBI3(«mkBuiltin(vm,i.bi,&intLess); i.params[0]=&vm.X[0]; i.params[1]=&vm.X[3]; i.params[2]=&vm.X[5]; i.liveXs=31;»)
ifX(«i.cond=&vm.X[5]; else1=&i.ifFalse; i.liveXs=63;»)
callBI3(«mkBuiltin(vm,i.bi,&intSum); i.params[0]=&vm.X[1]; i.params[1]=&vm.X[2]; i.params[2]=&vm.X[5]; i.liveXs=31;»)
copyXX(«i.dest=&vm.X[1]; i.src=&vm.X[2];»)
copyXX(«i.dest=&vm.X[2]; i.src=&vm.X[5];»)
copyXC(«i.dest=&vm.X[5]; mkSmallint(vm,i.src,1);»)
callBI3(«mkBuiltin(vm,i.bi,&intSum); i.params[0]=&vm.X[0]; i.params[1]=&vm.X[5]; i.params[2]=&vm.X[0]; i.liveXs=63;»)
callX(«i.callee=&vm.X[4]; i.arity=5;»)
copyXY(«i.dest=&vm.X[1]; i.src=0;»)
callBI1(«mkBuiltin(vm,i.bi,&printInt); i.params[0]=&vm.X[1]; i.liveXs=2;»)
deallocateY(«i.liveXs=0;»)
doPop(«i.liveXs=0;»)
callBI1(«*else1=w; mkBuiltin(vm,i.bi,&printInt); i.params[0]=&vm.X[3]; i.liveXs=8;»)
copyXY(«i.dest=&vm.X[1]; i.src=0;»)
callBI1(«mkBuiltin(vm,i.bi,&printInt); i.params[0]=&vm.X[1]; i.liveXs=2;»)
deallocateY(«i.liveXs=0;»)
doPop(«i.liveXs=0;»)
PP_CXX_VMA_END(«mkFibonacci»)

Instr** else2;
PP_CXX_VMA_BEGIN()
copyXC(«i.dest=&vm.X[2]; mkSmallint(vm,i.src,2);»)
callBI3(«mkBuiltin(vm,i.bi,&intLess); i.params[0]=&vm.X[0]; i.params[1]=&vm.X[2]; i.params[2]=&vm.X[2]; i.liveXs=7;»)
ifX(«i.cond=&vm.X[2]; else2=&i.ifFalse; i.liveXs=7;»)
copyXC(«i.dest=&vm.X[2]; mkSmallint(vm,i.src,1);»)
callBI2(«mkBuiltin(vm,i.bi,&unify); i.params[0]=&vm.X[1]; i.params[1]=&vm.X[2]; i.liveXs=6;»)
doPop(«i.liveXs=0;»)
allocateY(«*else2=w; i.count=3;»)
copyXC(«i.dest=&vm.X[2]; mkSmallint(vm,i.src,-1);»)
callBI3(«mkBuiltin(vm,i.bi,&intSum); i.params[0]=&vm.X[0]; i.params[1]=&vm.X[2]; i.params[2]=&vm.X[0]; i.liveXs=7;»)
callBI3(«mkBuiltin(vm,i.bi,&intSum); i.params[0]=&vm.X[0]; i.params[1]=&vm.X[2]; i.params[2]=&vm.X[3]; i.liveXs=7;»)
movYX(«i.dest=0; i.src=&vm.X[3];»)
movYX(«i.dest=1; i.src=&vm.X[1];»)
m4_dnl callBI1(«mkBuiltin(vm,i.bi,&makeVar); i.params[0]=&vm.X[1]; i.liveXs=3;»)
m4_dnl copyYX(«i.dest=2; i.src=&vm.X[1];»)
createVarXY(«i.dest1=&vm.X[1]; i.dest2=2;»)
copyXG(«i.dest=&vm.X[3]; i.src=0;»)
callX(«i.callee=&vm.X[3];» i.arity=2;)
movXY(«i.dest=&vm.X[0]; i.src=0;»)
m4_dnl callBI1(«mkBuiltin(vm,i.bi,&makeVar); i.params[0]=&vm.X[1]; i.liveXs=0;»)
m4_dnl copyYX(«i.dest=0; i.src=&vm.X[1];»)
createVarXY(«i.dest1=&vm.X[1]; i.dest2=0;»)
copyXG(«i.dest=&vm.X[3]; i.src=0;»)
callX(«i.callee=&vm.X[3]; i.arity=2;»)
movXY(«i.dest=&vm.X[0]; i.src=1;»)
movXY(«i.dest=&vm.X[1]; i.src=2;»)
movXY(«i.dest=&vm.X[2]; i.src=0;»)
deallocateY(«i.liveXs=7;»)
callBI3(«mkBuiltin(vm,i.bi,&intSum); i.params[0]=&vm.X[1]; i.params[1]=&vm.X[2]; i.params[2]=&vm.X[3]; i.liveXs=7;»)
callBI2(«mkBuiltin(vm,i.bi,&unify); i.params[0]=&vm.X[0]; i.params[1]=&vm.X[3]; i.liveXs=9;»)
doPop(«i.liveXs=0;»)
PP_CXX_VMA_END(«mkNaiveFibonacci»)

m4_define(«PP_FIB_VERSION»,«naive»)
PP_CXX_VMA_BEGIN()
//m4_ifelse(PP_FIB_VERSION,«linear»,«
copyXC(«i.dest=&vm.X[0]; mkSmallint(vm,i.src,1);»)
copyXX(«i.dest=&vm.X[1]; i.src=&vm.X[0];»)
copyXX(«i.dest=&vm.X[2]; i.src=&vm.X[0];»)
copyXG(«i.dest=&vm.X[3]; i.src=0;»)
copyXG(«i.dest=&vm.X[4]; i.src=1;»)
tailCallX(«i.callee=&vm.X[4];» i.arity=5;)
doPop(«i.liveXs=0;»)
//»)
//m4_ifelse(PP_FIB_VERSION,«naive»,«
allocateY(«i.count=1;»)
copyXG(«i.dest=&vm.X[0]; i.src=0;»)
callBI1(«mkBuiltin(vm,i.bi,&makeVar); i.params[0]=&vm.X[1]; i.liveXs=1;»)
copyYX(«i.dest=0; i.src=&vm.X[1];»)
copyXG(«i.dest=&vm.X[2]; i.src=2;»)
callBI1(«mkBuiltin(vm,i.bi,&printInt); i.params[0]=&vm.X[0]; i.liveXs=7;»)
callX(«i.callee=&vm.X[2]; i.arity=2;»)
copyXY(«i.dest=&vm.X[0]; i.src=0;»)
deallocateY(«i.liveXs=1;»)
callBI1(«mkBuiltin(vm,i.bi,&printInt); i.params[0]=&vm.X[0]; i.liveXs=1;»)
doPop(«i.liveXs=0;»)
//»)
PP_CXX_VMA_END(«mkMainCode»)
