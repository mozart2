void gcIntoSimpleVar(Node&, GC& gc, Node& dest){
  mkSimpleVar(gc.vm, dest);
}
const VTable simpleVarVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoSimpleVar»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoSimpleVar>»)
  //PP_VTABLE_PRODUCE
};
