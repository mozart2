const VTable vtUnimplemented={0,0,0,0,0,0};
void gcIntoNoValue(Node& it, GC&, Node& dest){
  dest.vt=it.vt;
  dest.c=it.c;
}
const VTable vtNoValue={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«copyable»,1)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoNoValue»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoNoValue>»)
  //PP_VTABLE_PRODUCE
};
