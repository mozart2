extern const VTable simpleBuiltinVT;
void mkBuiltin(VM &, Node &target, BIStatus* (*proc)(VM &vm, UnstableNode* args[])){
  target.vt=&simpleBuiltinVT;
  target.c.fptr=(void(*)())proc;
}
