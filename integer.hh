extern const VTable smallintVT;
void mkSmallint(VM &, Node &target, smw value){
  target.vt=&smallintVT;
  target.c.si=value;
}
