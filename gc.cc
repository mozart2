void GC::process(){
  while(stableTodo||unstableTodo||ptrStableTodo){
    if(stableTodo){
      Node &n=deref(*(Node*)stableTodo->c.ptr);
      StableNode* p=stableTodo;
      stableTodo=(StableNode*)stableTodo->gcNext;
      if(n.vt==&gcStableRefVT){
	set(*p,*(StableNode*)n.c.ptr);
      }else if(n.vt==&gcUnstableRefVT){
	set(*p,*(UnstableNode*)n.c.ptr);
      }else{
	n.vt->gcInto(n,*this,*p);
	n.vt=&gcStableRefVT;
	n.c.ptr=p;
      }
    }else if(unstableTodo){
      Node &n=deref(*(Node*)unstableTodo->c.ptr);
      UnstableNode* p=unstableTodo;
      unstableTodo=(UnstableNode*)unstableTodo->gcNext;
      if(n.vt==&gcStableRefVT){
	set(*p,*(StableNode*)n.c.ptr);
      }else if(n.vt==&gcUnstableRefVT){
	set(*p,*(UnstableNode*)n.c.ptr,vm);
      }else{
	n.vt->gcInto(n,*this,*p);
	n.vt=&gcUnstableRefVT;
	n.c.ptr=p;
      }
    }else /*if(ptrStableTodo)*/{
      StableNode &n=deref(**ptrStableTodo->nptr);
      StableNode *&p=*ptrStableTodo->nptr;
      ptrStableTodo=ptrStableTodo->next;
      if(n.vt==&gcStableRefVT){
	p=(StableNode*)n.c.ptr;
      }else if(n.vt==&gcUnstableRefVT){
	p=&stable(*(UnstableNode*)n.c.ptr,vm);
      }else{
	p=&(n.vt->gc(n,*this));
	n.vt=&gcStableRefVT;
	n.c.ptr=p;
      }
    }
  }
}
const VTable gcStableRefVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_PRODUCE
};
const VTable gcUnstableRefVT={
  //PP_VTABLE_DEF(«gcStableRefVT»)
  //PP_VTABLE_PRODUCE
};
template<StableNode& (*GC_FUNC)(Node&,GC&)>
void gcIntoDef(Node& it,GC& gc, Node& dest){
  set(dest, GC_FUNC(it, gc));
}
template<void (*GC_INTO_FUNC)(Node&,GC&,Node&)>
StableNode& gcDef(Node& it,GC& gc){
  StableNode &n=*new(gc.vm) StableNode();
  GC_INTO_FUNC(it,gc,n);
  return n;
}
