void gcIntoCodeBlock(Node& it, GC& gc, Node& dest){
  dest.vt=it.vt;
  CodeBlockHeader* t=(CodeBlockHeader*) it.c.ptr;
  CodeBlockHeader* h=(CodeBlockHeader*) operator new(sizeof(CodeBlockHeader)+t->length, gc.vm);
  dest.c.ptr=h;
  h->length=t->length;
  h->arity=t->arity;
  h->numG=t->numG;
  Instr* r=(Instr*)(t+1);
  Instr* r0=(Instr*)(t+1);
  Instr* e=(Instr*)(((byte*)r)+t->length);
  Instr* w=(Instr*)(h+1);
  Instr* w0=(Instr*)(h+1);
  while(r<e){
    //PP_FOR_EACH_INSTR(«
    if(*r==gc.vm.instrTable[I_«»PP_INSTR_NAME]){
      s_«»PP_INSTR_NAME &ri=*(s_«»PP_INSTR_NAME *)r;
      s_«»PP_INSTR_NAME &wi=*(s_«»PP_INSTR_NAME *)w;
      wi.instr=ri.instr;
      PP_FOR_EACH_INSTR_PARAM(«
  m4_ifelse(m4_defn(«PP_INSTR_PARAM_TYPE»),«WX»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«RX»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«WY»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«RY»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«RG»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«V»,
	    «new((void*)&wi.PP_INSTR_PARAM_NAME) StableNode(gc,ri.PP_INSTR_PARAM_NAME);»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«S»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«I»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME-r0+w0;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«PA»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_defn(«PP_INSTR_PARAM_TYPE»),«LX»,
	    «wi.PP_INSTR_PARAM_NAME=ri.PP_INSTR_PARAM_NAME;»,
	    m4_regexp(m4_defn(«PP_INSTR_PARAM_TYPE»),«X\[\([0-9]*\)\]»,
		      «for(unsigned int i=0;i<\1;++i){
			wi.PP_INSTR_PARAM_NAME[i]=ri.PP_INSTR_PARAM_NAME[i];
		      }»))»,«»)
      r+=sizeof(s_«»PP_INSTR_NAME)/sizeof(Instr);
      w+=sizeof(s_«»PP_INSTR_NAME)/sizeof(Instr);
    }
    //»,«else»)
    else{/*never reached*/}
  }
}
const VTable codeBlockVT={
  //PP_VTABLE_DEF(«vtUnimplemented»)
  //PP_VTABLE_SET(«copyable»,0)
  //PP_VTABLE_SET(«gcInto»,«&gcIntoCodeBlock»)
  //PP_VTABLE_SET(«gc»,«&gcDef<&gcIntoCodeBlock>»)
  //PP_VTABLE_PRODUCE
};
