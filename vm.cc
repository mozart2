struct XSet {
  size_t count;
  bool set[X_COUNT];
  bool mask[X_COUNT];
  XSet():count(0){
    for(unsigned int i=0;i<X_COUNT;++i){
      set[i]=false;
      mask[i]=true;
    }
  }
  void read(ptrdiff_t i){
    if(mask[i]&&!set[i]){
      set[i]=true;
      ++count;
    }
  }
  void write(ptrdiff_t i){
    mask[i]=false;
  }
  void arity(size_t s){
    for(unsigned int i=0;i<s;++i)read(i);
  }
  void finish(long s){
    if(s<0){
      for(int i=0;i<-s;++i)read(i);
    }else{
      int i=0;
      while(s){
	if(s&1)read(i);
	s>>=1;
	++i;
      }
    }
  }
};
bool VM::run(){
  if(!initialized){
    stackTable[S_CONT]=&&stackCont;
    stackTable[S_GC_CONT]=&&stackGCCont;
    stackTable[S_X]=&&stackX;
    //PP_FOR_EACH_INSTR(«
    instrTable[I_«»PP_INSTR_NAME]=&&lbl_«»PP_INSTR_NAME;
    //»,«»)
    //VMInit
    initialized=1;
    return 0;
  }
  do{
    UnstableNode curThread;
    VM &vm=*this;
    threads.get(vm, curThread);
    Instr* startIP=0;
    Instr* IP=0;
    UnstableNode* Y=0;
    size_t numY=0;
    StableNode* G=0;
    StackFrameHeader* stackTop=deref(curThread).vt->stackTop(deref(curThread));
    StableNode* wPtr=0;
    BIStatus* st=0;
    goto lbl_doPop;
    //PP_FOR_EACH_INSTR(«
    lbl_«»PP_INSTR_NAME:
      {
	s_«»PP_INSTR_NAME &i __attribute__((unused)) =*(s_«»PP_INSTR_NAME *)IP;
	IP+=sizeof(s_«»PP_INSTR_NAME)/sizeof(Instr);
	PP_INSTR_CODE;
	goto **IP;
      }
    //»,«»)
  specialStatus:
    //Do something
    goto lbl_doPop;
  stackGCCont:
    {
      GCContStackFrame &f=*(GCContStackFrame*)stackTop;
      stackTop=f.next;
      Y=f.Y;
      numY=f.numY;
      Node &n=deref(f.abstr);
      Instr* nIP;
      StableNode* nG;
      n.vt->call(n, nIP, nG, -1);
      startIP=nIP;
      IP=nIP+f.offset;
      G=nG;
      free(&f,1);
      if(scheduleNext())goto nextThread;
      goto **IP;
    }
  stackCont:
    {
      ContStackFrame &f=*(ContStackFrame*)stackTop;
      stackTop=f.next;
      startIP=f.startIP;
      IP=f.IP;
      Y=f.Y;
      numY=f.numY;
      G=f.G;
      free(&f,1);
      if(scheduleNext())goto nextThread;
      goto **IP;
    }
  stackX:
    {
      XStackFrame &f=*(XStackFrame*)stackTop;
      stackTop=f.next;
      for(unsigned int i=0;i<f.count;++i){
	SavedX &s=f.ptr[i];
	move(X[s.x],s.r);
      }
      free(f.ptr,f.count);
      free(&f,1);
      goto lbl_doPop;
    }
  stackEmpty:
    //Do something
    return 0;
  nextThread:
    {
      ContStackFrame *fc=alloc<ContStackFrame>(1);
      fc->kind=&&stackCont;
      fc->next=stackTop;
      fc->startIP=startIP;
      fc->IP=IP;
      fc->Y=Y;
      fc->numY=numY;
      fc->G=G;
      XSet live;
      while(1){
	//PP_FOR_EACH_INSTR(«
	if(*IP==&&lbl_«»PP_INSTR_NAME)
	  {
	    s_«»PP_INSTR_NAME &i __attribute__((unused)) =*(s_«»PP_INSTR_NAME *)IP;
	    IP+=sizeof(s_«»PP_INSTR_NAME)/sizeof(Instr);
	    m4_dnl /*
		     PP_FOR_EACH_INSTR_PARAM(«
		     m4_ifelse(m4_defn(«PP_INSTR_PARAM_TYPE»),«WX»,«live.write(i.»PP_INSTR_PARAM_NAME«-X);»,
		     m4_defn(«PP_INSTR_PARAM_TYPE»),«RX»,«live.read(i.»PP_INSTR_PARAM_NAME«-X);»,
		     m4_defn(«PP_INSTR_PARAM_TYPE»),«PA»,«live.arity(i.»PP_INSTR_PARAM_NAME«);break;»,
		     m4_defn(«PP_INSTR_PARAM_TYPE»),«LX»,«live.finish(i.»PP_INSTR_PARAM_NAME«);break;»)»
		     ,«»)
		     m4_dnl */
	      }
	else
	  //»,«»)
	  {/*should never be reached*/}
      }
      XStackFrame *fx=alloc<XStackFrame>(1);
      fx->kind=&&stackX;
      fx->next=fc;
      fx->count=live.count;
      fx->ptr=alloc<SavedX>(live.count);
      int j=0;
      for(unsigned int i=0;i<live.count;++i){
	while(!live.set[j])++j;
	SavedX &s=(fx->ptr)[i];
	s.x=j;
	move(s.r,X[j]);
	++j;
      }
      deref(curThread).vt->stackTop(deref(curThread))=fx;
      threads.schedule(vm, curThread);
    }
  }while(!interrupted());
  return 1;
}
void VM::pushCall(Node &thread, Node &proc){
  Node &t=deref(thread);
  StackFrameHeader* &stackTop=t.vt->stackTop(t);
  ContStackFrame *f=alloc<ContStackFrame>(1);
  Node &n=deref(proc);
  f->kind=stackTable[S_CONT];
  f->next=stackTop;
  f->Y=0;
  f->numY=0;
  n.vt->call(n,f->IP,f->G,0);
  f->startIP=f->IP;
  stackTop=f;
}
void VM::gc(){
  exchange(active,shadow);
  exchange(bigEmulAlloc,bigEmulAllocShadow);
  exchange(smallEmulAlloc,smallEmulAllocShadow);
  uniques.gc();
  GC* gc=allocShadow<GC>(1);
  new((void*)gc)GC(*this);
  threads.gc(*gc);
  gc->process();
  bigEmulAllocShadow.clear();
  smallEmulAllocShadow.clear();
  shadow.release();
}
void VM::gcStack(GC &gc, StackFrameHeader* &from, StackFrameHeader* &to){
  StackFrameHeader* f=from;
  StackFrameHeader** t=&to;
  size_t count=0;
  while(f){
    count++;
    if(f->kind==stackTable[S_CONT]){
      *t=alloc<GCContStackFrame>(1);
      GCContStackFrame &tt=*(GCContStackFrame*)*t;
      ContStackFrame &ff=*(ContStackFrame*)f;      
      tt.kind=stackTable[S_GC_CONT];
      tt.offset=ff.IP-ff.startIP;
      tt.numY=ff.numY;
      new((void*)&(tt.abstr)) UnstableNode(gc,ff.G[-1]);
      tt.Y=alloc<UnstableNode>(ff.numY);
      for(unsigned int i=0;i<ff.numY;++i){
	new((void*)&(tt.Y[i])) UnstableNode(gc,ff.Y[i]);
      }
      f=f->next; 
      t=&(tt.next);
      freeShadow(&ff,1);
    }else if(f->kind==stackTable[S_GC_CONT]){
      *t=alloc<GCContStackFrame>(1);
      GCContStackFrame &tt=*(GCContStackFrame*)*t;
      GCContStackFrame &ff=*(GCContStackFrame*)f;      
      tt.kind=ff.kind;
      tt.offset=ff.offset;
      tt.numY=ff.numY;
      new((void*)&(tt.abstr)) UnstableNode(gc,ff.abstr);
      tt.Y=alloc<UnstableNode>(ff.numY);
      for(unsigned int i=0;i<ff.numY;++i){
	new((void*)&(tt.Y[i])) UnstableNode(gc,ff.Y[i]);
      }
      f=f->next; 
      t=&(tt.next);
    }else if(f->kind==stackTable[S_X]){
      *t=alloc<XStackFrame>(1);
      XStackFrame &tt=*(XStackFrame*)*t;
      XStackFrame &ff=*(XStackFrame*)f;
      tt.kind=ff.kind;
      tt.count=ff.count;
      tt.ptr=alloc<SavedX>(ff.count);
      for(unsigned int i=0;i<ff.count;++i){
	tt.ptr[i].x=ff.ptr[i].x;
	new((void*)&(tt.ptr[i].r)) UnstableNode(gc,ff.ptr[i].r);
      }
      f=f->next; 
      t=&(tt.next);
      freeShadow(&ff,1);
    }else{/*never reached*/}
  }
  *t=0;
}
