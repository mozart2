struct Abstraction{
  Abstraction(GC& gc, Abstraction* a);
  Abstraction(size_t _arity, size_t _numG):arity(_arity), numG(_numG){}
  size_t arity;
  size_t numG;
  StableNode code;
  StableNode* G;
};
/*The block of G registers is constructed so that G[-1] is the abstraction itself so that it can be garbage collected from the stack frame.*/
extern const VTable abstractionVT;
StableNode* mkAbstraction(VM &vm, Node &target, StableNode &cb, size_t arity, size_t numG);
